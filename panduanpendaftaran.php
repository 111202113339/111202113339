<!DOCTYPE html>
<html>
<head>
	<title>PANDUAN PENDAFTARAN UNIVERSITAS TESLA</title>
	<link rel="stylesheet" type="text/css" href="aaa.css">
	<link href="logokampus.ico" rel="shortcut icon">
</head !1>
	<nav class="menu" !12>
		<label !21><a href="beranda.php"><img src="logokampus.png" alt="image" height="80px" width="80px"></a></label>
		<h1 !26>PANDUAN PENDAFTARAN<br> UNIVERSITAS TESLA</h1>
		<ul !33>
			<li !41><a href="beranda.php" class="animisi" !6>BERANDA</a></li>
		</ul>
	</nav>

	<div class="panpen"!260>
		<h3>● PANDUAN PENDAFTARAN UNIVERSITAS TESLA</h3>
			<P>
				Pendaftaran calon mahasiswa baru hanya dilakukan melalui Android Smartphone. Calon mahasiswa yang ingin melakukan pendataran terlebih dahulu harus mendownload aplikasinya di <i>Google Play Store.</i>
				. Setelah itu, silahkan lakukan pendaftaran dengan prosedur berikut :<br>
			</P>
			<div class="isipan">
				<h4>A. Pendaftaran Online</h4>
				<ol>
					<li>Buka aplikasi pendaftaran Untes di Android Smartphone</li>
					<li>Pilih Daftar</li>
					<li>Lengkapi form pendaftaran</li>
					<li>Jika pendaftaran berhasil, sistem akan mengirimkan SMS dan e-mail untuk langkah lebih lanjut</li>
					<li>Bayar biaya pendaftaran</li>
					<li>Jika pembayaran sukses, petunjuk untuk mengikuti ujian online mandiri akan dikirimkan melalui SMS dan e-mail</li>
					<br>
				</ol>
				<h4>B. Mengikuti Ujian Saringan Online Mandiri</h4>
				<ol>
					<li>Buka aplikasi lalu Login ke sistem dengan email dan nomor HP, lalu klik "Ujian Saringan"</li>
					<li>Jika berhasil (lulus), lakukan daftar ulang</li>
					<li>Ketentuan daftar ulang dan biaya yang harus dibayar dikirimkan via SMS dan email</li>
					<br>
				</ol>
				<h4>C. Daftar Ulang Online</h4>
				<ol>
					<li>Lakukan pembayaran biaya kuliah sesuai program studi dan pilihan waktu (lihat SMS atau email)</li>
					<li>Buka aplikasi PMB Untes di Android Smartphone anda lalu login dengan email dan nomor hp anda</li>
					<li>Lakukan pengisian form daftar ulang sesuai form yang disediakan</li>
					<li>Upload dokumen dengan sesuai</li>
					<br>
				</ol>
			</div>
	</div>
	<footer !77>
		<div class="cr"!83>©Copyright Tesla University 2021</div>
	</footer> 
</body>
</html>