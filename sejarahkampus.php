<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>SEJARAH UNIVERSITAS TESLA</title>
	<link rel="stylesheet" type="text/css" href="aaa.css">
	<link href="logokampus.ico" rel="shortcut icon">
</head>
<body !1>
	<nav class="menu" !12>
		<label !21><a href="beranda.php"><img src="logokampus.png" alt="image" height="80px" width="80px"></a></label>
		<h1 !26>SEJARAH <br> UNIVERSITAS TESLA</h1>
		<ul !33>
			<li !41><a href="beranda.php" class="animisi" !6>BERANDA</a></li>
		</ul>
	</nav>

	<div class="sjr"!53>
		<h3>● SEJARAH UNIVERSITAS TESLA</h3>
			<P>
				Universitas Tesla secara resmi terbentuk pada tahun 2021 melalui Keputusan Menteri Pendidikan Nasional Republik Indonesia nomor 9999/Z/Z/2021 tanggal 5 April 2021, sebagai pengembangan dari Sekolah Tinggi Informatika Tesla yang dimana kampus utamanya terletak di Kabupaten Kulonprogo, Yogyakarta. Nama Universitas Tesla sendiri memiliki sebuah arti filosofi dimana kata "Tesla" berasal dari nama seorang ilmuan besar di bidang teknologi bernama Nicola Tesla.<br>
			</P>
			<p>
				Universitas Tesla berdiri atas dasar cepat dan melesatnya perkembangan teknologi informasi dan komunikasi. Seiring melesatnya perkembangan teknologi informasi dan komunikasi maka semakin tinggi pula permintaan pengguna dari teknologi tersebut. Semakin tingginya permintaan pengguna tidak sebanding dengan banyaknya pencipta dan pengembang dari teknologi informasi dan komunikasi tersebut yang permintaanya tidak sebesar penggunanya. Apabila hal tersebut berlarut-larut maka teknologi informasi dan komunikasi yang sedang berkembang pesat lama-kelamaan akan menurun perkembangannya dan bahkan akan mencapai titik kepunahan daripada teknologi informasi dan komunikasi tersebut. Atas dasar tersebut berdirilah Universitas Tesla yang bertujuan untuk mencetak para pencipta dan pengembang baru pada bidang Teknologi Informasi dan Komunikasi tersebut agar Teknologi Informasi dan Komunikasi dapat terus berkembang dengan pesat seiring dengan banyaknya permintaan pengguna.
			</P>
	</div>
	<footer !77>
		<div class="cr"!83>©Copyright Tesla University 2021</div>
	</footer>
</body>
</html>