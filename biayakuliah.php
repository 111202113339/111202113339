<!DOCTYPE html>
<html>
<head>
	<title>BIAYA KULIAH UNIVERSITAS TESLA</title>
	<link rel="stylesheet" type="text/css" href="aaa.css">
	<link href="logokampus.ico" rel="shortcut icon">
</head>
<body !1>
	<nav class="menu" !12>
		<label !21><a href="beranda.php"><img src="logokampus.png" alt="image" height="80px" width="80px"></a></label>
		<h1 !26>BIAYA KULIAH<br> UNIVERSITAS TESLA</h1>
		<ul !33>
			<li !41><a href="beranda.php" class="animisi" !6>BERANDA</a></li>
		</ul>
	</nav>

	<div class="bikul" !181>
		<h3>● BIAYA KULIAH UNIVERSITAS TESLA</h3>
			<p>Berikut biaya kuliah Universitas Tesla :</p>
		<table border="15px" bgcolor="skyblue" cellpadding="5">
			<tr>
				<th>FAKULTAS</th>
				<th>PROGRAM STUDI</th>
				<th>BIAYA PER SEMESTER</th>
			</tr>
			<tr>
				<th rowspan="7">TEKNIK & INFORMATIKA</th>
				<td>Teknologi Informasi</td>
				<td>Rp12.250.000,00</td>
			</tr>
			<tr>
				<td>Rekayasa Perangkat Lunak</td>
				<td>Rp11.250.000,00</td>
			</tr>
			<tr>
				<td>Sistem Informasi</td>
				<td>Rp12.250.000,00</td>
			</tr>
			<tr>
				<td>Sistem Informasi Akuntansi</td>
				<td>Rp13.250.000,00</td>
			</tr>
			<tr>
				<td>Teknik Elektro</td>
				<td>Rp12.000.000,00</td>
			</tr>
			<tr>
				<td>Teknik Industri</td>
				<td>Rp13.250.000,00</td>
			</tr>
			<tr>
				<td>Ilmu Komputer</th>
				<td>Rp12.000.000,00</td>
			</tr>
			<tr>
				<th rowspan="4">KOMUNIKASI & BAHASA</th>
				<td>Ilmu Komunikasi</td>
				<td>Rp 9.250.000,00</td>
			</tr>
			<tr>
				<td>Sastra Indonesia</td>
				<td>Rp10.250.000,00</td>
			</tr>
			<tr>
				<td>Sastra Inggris</td>
				<td>Rp 9.250.000,00</td>
			</tr>
			<tr>
				<td>Periklanan</td>
				<td>Rp11.250.000,00</td>
			</tr>
			<tr>
				<th rowspan="4">EKONOMI & BISNIS</th>
				<td>Manajemen</td>
				<td>Rp10.250.000,00</td>
			</tr>
			<tr>
				<td>Akuntansi</td>
				<td>Rp13.000.000,00</td>
			</tr>
			<tr>
				<td>Administrasi Perkantoran</td>
				<td>Rp11.250.000,00</td>
			</tr>
			<tr>
				<td>Administrasi Bisnis</td>
				<td>Rp11.250.000,00</td>
			</tr>
		</table>
	</div>
	<footer>
		<div class="cr"!83>©Copyright Tesla University 2021</div>
	</footer> 
</body>
</html>