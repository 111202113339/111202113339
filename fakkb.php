<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>FAKULTAS KOMUNIKASI & BAHASA UNIVERSITAS TESLA</title>
	<link rel="stylesheet" type="text/css" href="aaa.css">
	<link href="logokampus.ico" rel="shortcut icon">
</head>
<body !1>
	<nav class="menu" !12>
		<label !21><a href="beranda.php"><img src="logokampus.png" alt="image" height="80px" width="80px"></a></label>
		<h1 !26>FAKULTAS KOMUNIKASI & BAHASA <br> UNIVERSITAS TESLA</h1>
		<ul !33>
			<li !41><a href="beranda.php" class="animisi" !6>BERANDA</a></li>
		</ul>
	</nav>
	<div class="kombah"!137>
		<h3>● FAKULTAS KOMUNIKASI & BAHASA UNIVERSTAS TESLA</h3>
			<P>
				Fakultas Komunikasi & Bahasa Universitas Tesla terdiri dari beberapa program studi. Diantarannya :
				<br>
				<br>
			</P>
			<h4>1. Ilmu Komunikasi (S1)</h4>
				<p>
					Komunikasi adalah suatu kajian ilmu yang sangat luas dan intens, pasalnya dalam kehidupan sehari-hari kita tidak pernah lepas dari komunikasi. Tapi jangan kira kalau ilmu komunikasi itu cuma pelajaran ngomong doang! Tahu nggak, kalau komunikasi itu ada banyak macam nya, mulai dari komunikasi intrapersonal (dengan diri sendiri), komunikasi interpersonal (dengan orang lain), komunikasi kelompok, komunikasi organisasi, hingga komunikasi massa. Ilmu Komunikasi ini adalah induk dari ilmu turunannya yang lain seperti Jurnalistik, Penyiaran, Hubungan Masyarakat (Public Relation), Komunikasi Pemasaran, Desain Komunikasi Visual, Periklanan (Adveritising), Performing Arts Communication, dan lain-lain. Pada program studi Ilmu Komunikasi kita diajarkan tentang bagaimana proses penyampaian pesan agar menjadi efektif dan dapat mencapai sasaran yang dituju. Lulusan program studi Ilmu Komunikasi akan mendapatkan gelar Sarjana Ilmu Komunkasi (S.I.Kom).<br>
				</p>

			<h4>2. Sastra Indonesia (S1)</h4>
				<p>
					Program studi ini khusus mempelajari tren sastra di Indonesia. Para mahasiswa akan mempelajari beraneka ragam subyek sastra, puisi, drama, roman-novel, dan linguistik. Pada semester 4, mahasiswa akan diarahkan untuk memilih beberapa konsentrasi yaitu peminatan sastra, peminatan linguistik, dan peminatan filologi.<br>
				</p>

			<h4>3. Sastra Inggris (S1)</h4>
				<p>
					Dengan memilih program studi Sastra Inggris, kamu bisa mempelajari tentang isi, ciri artistik, estetika, keaslian, serta keindahan isi dalam teks atau tulisan karya sastra Inggris. Kamu akan mempelajari berbagai karya tulis baik dalam bentuk novel, naskah drama, atau puisi yang pasti ditulis dalam bahasa Inggris. Selain itu kamu juga akan mempelajari linguistik bahasa Inggris secara lebih mendalam. Walaupun mempelajari Sastra Inggris bertujuan untuk mengasah kemampuan analisis dengan mencermati berbagai karya sastra, namun tidak menutup kemungkinan kamu bisa menciptakan karya sastra.<br>
				</p>

			<h4>4. Periklanan (S1)</h4>
				<p>
					Program studi Periklanan (Advertising) mempelajari tentang cara menerapkan strategi kreatif periklanan melalui ilustrasi, animasi, desain komunikasi visual, sketsa dan topografi periklanan.<br>
				</p>
	</div>			
	<footer>
		<div class="cr"!83>©Copyright Tesla University 2021</div>
	</footer> 
</body>
</html>