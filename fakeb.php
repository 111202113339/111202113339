<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>FAKULTAS EKONOMI BISNIS UNIVERSITAS TESLA</title>
	<link rel="stylesheet" type="text/css" href="aaa.css">
	<link href="logokampus.ico" rel="shortcut icon">
</head>
<body>
	<nav class="menu" !12>
		<label !21><a href="beranda.php"><img src="logokampus.png" alt="image" height="80px" width="80px"></a></label>
		<h1 !26>FAKULTAS EKONOMI & BISNIS <br> UNIVERSITAS TESLA</h1>
		<ul !33>
			<li !41><a href="beranda.php" class="animisi" !6>BERANDA</a></li>
		</ul>
	</nav>
	<div class="ekbis"!159>
		<h3>● FAKULTAS EKONOMI & BISNIS UNIVERSTAS TESLA</h3>
			<P>
				Fakultas Ekonomi & Bisnis Universitas Tesla terdiri dari beberapa program studi. Diantarannya :
				<br>
				<br>
			</P>
			<h4>1. Manajemen (S1)</h4>
				<p>	
					Manajemen merupakan program studi yang mempelajari mengenai bagaimana mengelola suatu perusahaan atau organisasi. Manajemen juga termasuk dalam bidang bisnis dan ekonomi, tetapi lebih terfokus pada kegiatan mengelola, merencanakan, dan mengatur (manajemen) semua proses dalam perusahaan untuk mencapai tujuan.<br>
				</p>

			<h4>2. Akuntansi (S1)</h4>
				<p>
					Akuntasi adalah sebuah program studi yang menyiapkan mahasiswanya untuk bisa mempraktekkan profesi akuntansi dan melakukan fungsi bisnis terkait, termasuk menerapkan instruksi dalam prinsip akuntansi dan teori, akuntansi keuangan, akuntansi manajerial, akuntansi biaya, pengendalian anggaran, akuntansi pajak, aspek hukum akuntansi, audit, prosedur pelaporan, analisis laporan, perencanaan dan konsultasi, sistem informasi bisnis, metode penelitian akuntansi, standar profesional dan etika. Ilmu Akuntansi lalu diaplikasikan di berbagai instansi bisnis, baik yang bersifat profit, non-profit, publik, maupun instansi pemerintahan.<br>
				</p>

			<h4>3. Administrasi Perkantoran (S1)</h4>
				<p>
					Program studi Administrasi Perkantoran atau disebut juga Kesekretariatan adalah program studi yang berfokus pada perencanaan anggaran keuangan, pembayaran dan pendataan suatu oranisasi atau perkantoran atau perusahaan.<br>
				</p>

			<h4>4. Administrasi Bisnis (S1)</h4>
				<p>
					Program studi Administrasi Bisnis atau yang kerap disebut sebagai Administrasi Niaga akan mengajakmu untuk mempelajari segala sesuatu terkait kegiatan operasional bisnis dan perusahaan. Kegiatan-kegiatan tersebut seperti kegiatan pemasaran (marketing), pengelolaan keuangan, pengelolaan personalia (SDM), hingga kegiatan produksi. Tidak hanya belajar menjalankan bisnis, kamu juga akan belajar untuk menciptakan sebuah produk sendiri. Bisnis yang dimaksud bukan sekedar menjual kembali produk dan mengambil keuntungan, melainkan menciptakan value pada produk yang akan dipasarkan. Administrasi Bisnis pastinya juga mempelajari segala urusan klarikal kantor, mengelola sarana dan prasarana kantor, memproses data secara akurat, dan bahkan mengelola informasi yang berhubungan dengan pekerjaan kantor.<br>
				</p>
	</div>			
	<footer>
		<div class="cr"!83>©Copyright Tesla University 2021</div>
	</footer> 
</body>
</html>