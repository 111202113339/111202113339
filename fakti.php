<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>FAKULTAS TEKNIK & INFORMATIKA UNIVERSITAS TESLA</title>
	<link rel="stylesheet" type="text/css" href="aaa.css">
	<link href="logokampus.ico" rel="shortcut icon">
</head>
<body !1>
	<nav class="menu" !12>
		<label !21><a href="beranda.php"><img src="logokampus.png" alt="image" height="80px" width="80px"></a></label>
		<h1 !26>FAKULTAS TEKNIK & INFORMATIKA <br> UNIVERSITAS TESLA</h1>
		<ul !33>
			<li !41><a href="beranda.php" class="animisi" !6>BERANDA</a></li>
		</ul>
	</nav>
	<div class="tekin"!115>
		<h3>● FAKULTAS TEKNIK & INFORMATKA UNIVERSTAS TESLA</h3>
			<P>
				Fakultas Teknik & Informatika Universitas Tesla terdiri dari beberapa program studi. Diantarannya :
				<br>
				<br>
			</P>
			<h4>1. Teknologi Informasi (S1)</h4>
				<p>
					Teknologi informasi adalah sebuah jurusan yang mempelajari komponen penyusun hardware, software, dan brainware. Dimana Teknologi informasi bisa diartikan sebagai sebuah perangkat, baik perangkat keras maupun lunak, yang digunakan untuk mendukung pengolahan data dan informasi secara cepat dan berkualitas.<br>
				</p>

			<h4>2. Rekayasa Perangkat Lunak (S1)</h4>
				<p>
					RPL adalah sebuah jurusan yang mempelajari dan mendalami semua cara-cara pengembangan perangkat lunak termasuk pembuatan, pemeliharaan, manajemen organisasi pengembangan perangkat lunak dan manajemen kualitas.<br>
				</p>

			<h4>3. Sistem Informasi (S1)</h4>
				<p>
					Sistem Informasi adalah bidang yang menggabungkan ilmu komputer dengan bisnis dan manajemen. Di jurusan ini kamu akan belajar gimana mengidentifikasi kebutuhan dan proses bisnis perusahaan berdasarkan data-data yang dimiliki perusahan, kemudian merancang sistem yang sesuai dengan kebutuhan perusahaan.<br>
				</p>

			<h4>4. Sistem Infromasi Akuntansi (S1)</h4>
				<p>
					Pada program studi Accounting Informastion System kita akan mendapatkan ilmu Sistem Informasi dipadukan dengan Akuntansi. Banyaknya praktisi di bidang IT, dan berkembangnya organisasi-organisasi IT mendesak para praktisi IT untuk turut memahami ilmu manajemen dan keuangan untuk dapat menjalankan organisasi nya dengan lebih baik. Program studi ini akan membekali kita dengan dasar-dasar akuntansi dan bisnis yang dibutuhkan oleh seorang sistem analis, serta ilmu untuk merancang dan membangun sistem informasi berbasis komputer melalui studi sistem informasi untuk memenuhi tujuan strategis perusahaan. Accounting Information System (Sistem Informasi Akuntansi) ini dirancang untuk mengolah transaksi seperti kas, biaya produksi, gaji, serta utang-piutang, menjadi sebuah laporan.<br>
				</p>
		
			<h4>5. Teknik Elektro (S1)</h4>
				<p>
					Teknik elektro merupakan ilmu teknik yang mempelajari sifat-sifat elektron yang kita kenal sebagai listrik. Mahasiswa Teknik Elektro mempelajari aplikasi dan pemanfaatan listrik dalam kehidupan sehari-hari, serta teknologi yang terkait. Cakupan teknik elektro sangat luas, mulai dari sumber pembangkit tenaga listrik, sistem jaringan distribusi, sampai pada pemanfaatannya oleh pengguna akhir. Teknik Elektro pun dipecah menjadi beberapa konsentrasi, seperti teknik elektronika, teknik listrik, teknik instrumentasi, sampai teknik komputer dan telekomunikasi.<br>
				</p>

			<h4>6. Teknik Industri (S1)</h4>
				<p>
					Teknik Industri merupakan ilmu yang mempelajari proses industri baik dari sisi manajemen ataupun teknik. Ilmu teknik industri sebenarnya adalah turunan dari teknik mesin. Walau demikian, para mahasiswa juga harus mempelajari disiplin lain seperti matematika, fisika, fisiologi, bahkan manajemen saintifik. Teknik industri berfokus kepada perancangan, peningkatan, dan pemasangan sistem terintegrasi yang membutuhkan peran manusia, material, peralatan dan energi. Teknik Industri sendiri dibagi menjadi tiga bidang besar. Pertama, sistem manufaktur yang mempelajari peningkatan kualitas, produktivitas, dan efisiensi sistem produksi. Kedua adalah manajemen industri, di mana para mahasiswa harus mempelajari manajemen keuangan, manajemen operasional, manajemen inovasi, perencanaan dan pengendalian produksi, dan ekonomi teknik. Ketiga adalah sistem industri dan tekno ekonomi, seperti logistik, statistik, penelitian operasional, dan sistem basis data.<br>
				</p>

			<h4>7. Ilmu Komputer (S1)</h4>
				<p>
					Ilmu Komputer adalah sebuah ilmu yang mempelajari baik tentang komputasi, perangkat keras (hardware) maupun perangkat lunak (software). Ilmu komputer mencakup beragam topik yang berkaitan dengan komputer, mulai dari analisis abstrak algoritma sampai subyek yang lebih konkret seperti bahasa pemrograman, perangkat lunak, termasuk perangkat keras. Berbeda dengan Teknik Informatika yang lebih menekankan pada sisi penerapannya, program studi Ilmu Komputer lebih fokus pada 'sains' dari bidang komputer itu sendiri.<br>
				</p>
	</div>			
	<footer !77>
		<div class="cr"!83>©Copyright Tesla University 2021</div>
	</footer> 
</body>
</html>